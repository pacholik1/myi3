#!/usr/bin/python3

# Copyright (C) 2018  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

"""Wrapper around lemonbar with asynchronous functions that feed it"""
# deps: allumik's lemonbar-xft (https://github.com/allumik/lemonbar-xft)
# i3ipc, dbus-next, sh, psutil

# import os
import sys
import asyncio
from datetime import datetime, timedelta
import sh
import i3ipc
import i3ipc.aio
import dbus_next
import psutil


class Feeder:
    FONT = 'xft:DejaVu Sans Mono for Powerline:size=10'
    FOREGROUND = '#ebdbb2'
    BACKGROUND = '#282828'

    def __init__(self):
        self.bar_p = None

    async def initialize(self):
        self.bar_p = await asyncio.create_subprocess_exec(
            'lemonbar',
            # {width}x{height}+{xoffset}+{yoffset}
            # '-g', '0x20+0+100',
            '-R', '-d',
            '-f', self.FONT,
            # '-u', '5',
            '-B', self.BACKGROUND,
            '-F', self.FOREGROUND,
            stdin=asyncio.subprocess.PIPE,
            stdout=asyncio.subprocess.PIPE,
        )
        self.i3 = await i3ipc.aio.Connection().connect()

    def __getattr__(self, name):
        try:
            return super(Feeder, self).__getattr__(name)
        except AttributeError:
            return '-'

    @staticmethod
    def invert_colors(block: str):
        return (
            '%{F'f'{Feeder.BACKGROUND}''}%{B'f'{Feeder.FOREGROUND}''}'
            f'{block}'
            '%{B-}%{F-}'
        )

    @staticmethod
    def create_workspace_switcher(cur_desktop: int):
        one_workspace = (
            '%{A:workspace X:}'
            '%{O3}'
            'X'
            '%{O3}'
            '%{A}'
        )
        return ''.join(
            (Feeder.invert_colors if cur_desktop == i else str)(
                one_workspace.replace('X', str(i))
            )
            for i in range(1, 10))

    def create_slider(self, percent):
        try:
            return '▁▂▃▄▅▆▇█'[int(percent/12.501)]
        except TypeError:
            return ' '

    def feed_sync(self):
        self.bar_p.stdin.write((
            '%{l}%{O10}'
            f'{self.create_workspace_switcher(self.cur_desktop)}'
            '%{O10}'
            f'{self.spotify}'
            '%{r}'
            f'{self.create_slider(self.bat)}''%{O5}'
            f'{self.create_slider(self.cpu)}''%{O5}'
            f'{self.create_slider(self.mem)}''%{O5}'
            f'{self.create_slider(self.wifi_signal)}''%{O5}'
            '%{O10}'
            f'{self.clock}'
            '%{O10}'
            '\n'
        ).encode())

    async def feed(self):
        self.feed_sync()
        await self.bar_p.stdin.drain()

    async def consume(self):
        while True:
            command = await self.bar_p.stdout.readline()
            # print(command.decode('utf-8'), end='')
            await self.i3.command(command.decode())
            # asyncio.create_task(self.feed())


feeder = Feeder()


async def countdown(count):
    end = datetime.now() + timedelta(seconds=count)

    for c in range(count, 0, -1):
        feeder.countdown = c
        await feeder.feed()
        td = end - datetime.now() - timedelta(seconds=c-1)
        await asyncio.sleep(td.total_seconds())

    feeder.countdown = 0
    sys.exit()


async def clock():
    while True:
        d = datetime.now()
        td = (datetime(d.year, d.month, d.day, d.hour, d.minute) +
              timedelta(minutes=1) - d)
        feeder.clock = d.strftime('%a %d.%m. %H:%M')
        await feeder.feed()
        await asyncio.sleep(td.total_seconds())


async def i3watch(i3: i3ipc.aio.Connection):
    # tree = await i3.get_tree()
    # focused = tree.find_focused()
    # workspace = focused.workspace()
    # feeder.cur_desktop = workspace.num
    feeder.cur_desktop = 1
    await feeder.feed()

    async def _on_i3_change(i3: i3ipc.aio.Connection,
                            e: i3ipc.events.WorkspaceEvent):
        feeder.cur_desktop = e.current.num
        await feeder.feed()

    i3.on(i3ipc.Event.WORKSPACE_FOCUS, _on_i3_change)
    asyncio.create_task(i3.main())


async def spotify():
    bus = await dbus_next.aio.MessageBus().connect()
    introspection = await _stubborn(
        bus.introspect,
        'org.mpris.MediaPlayer2.spotify', '/org/mpris/MediaPlayer2')
    obj = bus.get_proxy_object(
        'org.mpris.MediaPlayer2.spotify', '/org/mpris/MediaPlayer2',
        introspection)
    properties = obj.get_interface('org.freedesktop.DBus.Properties')

    player = obj.get_interface('org.mpris.MediaPlayer2.Player')
    metadata = await player.get_metadata()
    artists = ", ".join(
        metadata['xesam:artist'].value
    )
    title = metadata['xesam:title'].value
    feeder.spotify = f"{artists} – {title}"
    await feeder.feed()

    def on_properties_changed(interface_name, changed_properties,
                              invalidated_properties):
        artists = ", ".join(
            changed_properties['Metadata'].value['xesam:artist'].value
        )
        title = changed_properties['Metadata'].value['xesam:title'].value
        feeder.spotify = f"{artists} – {title}"
        feeder.feed_sync()

    properties.on_properties_changed(on_properties_changed)


async def wireless():
    while True:
        out = sh.wicd_cli(wireless=True, status=True).stdout.decode()
        try:
            status, tpe, info, *_ = out.splitlines()
        except ValueError:
            pass
        else:
            wired_wireless = tpe.split()[2]

            if wired_wireless == 'Wireless':
                feeder.wifi_signal = int(info.split()[4][:-1])
            await feeder.feed()
        await asyncio.sleep(10)


async def sliders():
    while True:
        try:
            feeder.bat = psutil.sensors_battery().percent
        except AttributeError:
            # feeder.bat = 100
            pass
        feeder.cpu = psutil.cpu_percent()
        feeder.mem = psutil.virtual_memory().percent
        await feeder.feed()
        await asyncio.sleep(10)


async def _stubborn(f, *args):
    while True:
        try:
            result = await f(*args)
        except Exception:
            await asyncio.sleep(10)
        else:
            return result


async def main():
    await feeder.initialize()
    await asyncio.gather(
        clock(),
        i3watch(feeder.i3),
        spotify(),
        wireless(),
        sliders(),
        feeder.consume(),
    )


if __name__ == '__main__':
    asyncio.run(main(*sys.argv[1:]))
