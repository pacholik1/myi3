#!/usr/bin/python3

# Copyright (C) 2020  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

import sys
import asyncio
import i3ipc
import i3ipc.aio


async def get_workspace(i3: i3ipc.aio.Connection):
    tree = await i3.get_tree()
    focused = tree.find_focused()
    workspace = focused.workspace()
    return workspace


async def move_right(i3: i3ipc.aio.Connection, container: i3ipc.Con):
    while True:
        workspace = await get_workspace(i3)
        if not workspace.nodes:
            continue
        rightmost = workspace.nodes[-1]
        if not rightmost.find_by_id(container.id):
            await container.command('move right')
        else:
            break


async def on_new_window(i3: i3ipc.aio.Connection, e: i3ipc.events.WindowEvent):
    workspace = await get_workspace(i3)
    num_nodes = len(workspace.nodes)
    if num_nodes == 0:
        return
    if num_nodes == 1:
        if e.container.layout != 'splith':
            # await e.container.command('split horizontal')
            pass
    if num_nodes > 1:
        right = workspace.nodes[-1]
        if right.layout != 'splitv':
            await right.command('split vertical')
    if num_nodes > 2:
        await move_right(i3, e.container)


async def main():
    i3 = await i3ipc.aio.Connection().connect()
    i3.on(i3ipc.Event.WINDOW_NEW, on_new_window)

    await i3.main()


if __name__ == '__main__':
    asyncio.run(main(*sys.argv[1:]))
